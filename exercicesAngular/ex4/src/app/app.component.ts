import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ex4';

  animaux = [
    {
      photo: 'https://www.18h39.fr/wp-content/uploads/2019/04/chat-trop-chou-600x420.jpg',
      nom: 'Chat',
      desc: "Le chat est un mammifère de la famille de félidés. Il pèse en moyenne 2,5 à 4,5 kg et mesure entre 66 et 76 cm, avec la queue. Ses pattes sont pourvues de griffes rétractiles. Elles ont à leur base des coussinets constitués d'une membrane élastique qui leur permet de se déplacer sans faire de bruit.",
      cri: 'Miaou'
    },
    {
      photo: 'https://www.notretemps.com/cache/com_zoo_images/c9/nom-chiot_6d24a44af72d79896fcaf95494afbff4.jpg',
      nom: 'Chien',
      desc: "Résultat de recherche d'images pour 'chien description' Le chien est un mammifère carnivore de la famille des canidés, issu du loup, le Canis lupus. Son nom scientifique est d'ailleurs Canis lupus familiaris. Familiaris parce qu'il a été domestiqué par l'Homme il y a plus de 30.000 ans. ... Ainsi le squelette du chien compte quelque 300 os.",
      cri: 'Wouf'
    },
    {
      photo: 'https://static.latribune.fr/full_width/777378/peter-gyongyosi-balabit-cybersecurite-intelligence-artificielle.jpg',
      nom: 'Humain',
      desc: "Qui possède les caractéristiques spécifiques de l'homme en tant que représentant de son espèce ; qui est composé d'hommes : Être humain. L'espèce humaine. Qui est relatif à l'homme, qui lui est propre : Corps humain.",
      cri: 'Nique ta mère'
    },
  ];
}
