import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AnimauxComponent } from './animaux/animaux.component';
import { FormsModule } from '@angular/forms';
import { TruncatePipesModule } from 'angular-truncate-pipes';

@NgModule({
  declarations: [
    AppComponent,
    AnimauxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    TruncatePipesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
