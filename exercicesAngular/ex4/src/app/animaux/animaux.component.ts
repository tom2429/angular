import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-animaux',
  templateUrl: './animaux.component.html',
  styleUrls: ['./animaux.component.scss']
})
export class AnimauxComponent implements OnInit {

  @Input() photo: string;
  @Input() nom: string;
  @Input() desc: string;
  @Input() cri: string;

  constructor() { }

  ngOnInit(): void {
  }


  alerteCri(cri: string){
    window.alert(cri);
  }
}
