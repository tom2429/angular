import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() photo: string;
  @Input() titre: string;
  @Input() desc: string;

  constructor() { }

  ngOnInit(): void {
  }

  

}
