import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tableau',
  templateUrl: './tableau.component.html',
  styleUrls: ['./tableau.component.scss']
})
export class TableauComponent implements OnInit {

  voitures=[
    {
      id: 0,
      marque: "renault",
      modele: "clio"
    },
    {
      id: 1,
      marque: "renault",
      modele: "twingo"
    },
    {
      id: 2,
      marque: "citroen",
      modele: "picasso"
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
