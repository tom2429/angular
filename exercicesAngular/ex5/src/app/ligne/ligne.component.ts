import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: '[app-ligne]',
  templateUrl: './ligne.component.html',
  styleUrls: ['./ligne.component.scss']
})
export class LigneComponent implements OnInit {

  @Input() id: number;
  @Input() marque: string;
  @Input() modele: string;

  details=[
    {
      photo: "https://fr.wikipedia.org/wiki/Renault_Clio_IV#/media/Fichier:Renault_Clio_TCe_90_Luxe_(IV)_%E2%80%93_Frontansicht,_17._Mai_2013,_M%C3%BCnster.jpg",
      titre: "clio",
      desc: "La Renault Clio IV (nom de code X98) est une citadine polyvalente du constructeur automobile français Renault produite à partir de 2012. Il s'agit de la quatrième génération de Clio après la Clio I en 1990, la Clio II en 1998 et Clio III en 2005. Avec plus de 100 000 exemplaires par an, elle est la voiture la plus vendue en France en 20137, 20148, 20159, 201610, 201711 et 2018, devant sa principale rivale la Peugeot 208, de gamme similaire. "
    },
    {
      photo: "https://i.gaw.to/content/photos/42/03/420392-essai-du-grenier-renault-twingo-2001.jpg",
      titre: "twingo",
      desc: "La Renault Twingo (en anglais : [twɪŋɡoʊ]) est une gamme d'automobile citadine du constructeur français Renault. Elle sera lancée en 1993 (Twingo I), puis en 2007 (Twingo II) et renouvelée depuis 2014 (Twingo III). Une version électrique nommée Twingo ZE est présentée peu avant le Salon de l'automobile de Genève 2020. Celle-ci a remplacé la Renault 4 produite depuis 1961. La Twingo demeure la citadine la plus vendue de tous les temps."
    },
    {
      photo: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Citro%C3%ABn_Xsara_Picasso_20090221_front.jpg/252px-Citro%C3%ABn_Xsara_Picasso_20090221_front.jpg",
      titre: "picasso",
      desc:"Le Citroën Xsara Picasso est une automobile produite par Citroën de 1999 à 2010 en Europe et de 1999 à 2012 en Amérique du Sud. Il s'agissait du premier monospace compact de la marque Citroën."
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

  getDetail(id:number){
    return this.details[id];
  }
}
