import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <div class="container"> 
    <h1>Un test de template en ligne</h1>
    <app-panier>Mon panier à du mal !</app-panier>
    <p> Blabla {{ title }} blabla</p>
    <app-stock>Mon text 1</app-stock>
    <app-stock>Mon text 2</app-stock>
    <app-stock>Mon text 3</app-stock>
    <app-stock> </app-stock>
  </div>
  `,
  styleUrls: []
})
export class AppComponent {
  title = 'Mon Projet';
}
