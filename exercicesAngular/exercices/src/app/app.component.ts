import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'exercices';

  titre = 'Titre de la carte';
  sousTitre = 'Sous titre de la carte';
  desc = 'description de la carte';
  lien = 'lien de la carte';
}
