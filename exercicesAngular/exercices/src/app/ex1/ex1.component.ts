import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-ex1',
  templateUrl: './ex1.component.html',
  styleUrls: ['./ex1.component.scss']
})
export class Ex1Component implements OnInit {

  @Input() titre: string;
  @Input() sousTitre: string;
  @Input() desc: string;
  @Input() lien: string;

  constructor() { }

  ngOnInit(): void {
  }

}
