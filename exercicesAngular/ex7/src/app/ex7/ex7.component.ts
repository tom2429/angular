import { Component, OnInit } from '@angular/core';
import {OtherService} from '../services/other.service';

@Component({
  selector: 'app-ex7',
  templateUrl: './ex7.component.html',
  styleUrls: ['./ex7.component.scss']
})
export class Ex7Component implements OnInit {
  
  elements: string[] = [];

  constructor(private otherService: OtherService) { }

  ngOnInit(): void {
    this.elements = this.otherService.exercice7Data;
  }

  onAddItem(): void {
    this.otherService.addItemExo7();
  }

}
