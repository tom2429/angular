import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputModifierComponent } from './input-modifier.component';

describe('InputModifierComponent', () => {
  let component: InputModifierComponent;
  let fixture: ComponentFixture<InputModifierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputModifierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputModifierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
