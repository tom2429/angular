import { Component, Input, OnInit } from '@angular/core';
import { OtherService } from '../../services/other.service';

@Component({
  selector: '[app-input-modifier]',
  templateUrl: './input-modifier.component.html',
  styleUrls: ['./input-modifier.component.scss']
})
export class InputModifierComponent implements OnInit {
  
  @Input() index: number = 0;
  name: string;
  editName: string;

  enableEdit: boolean = false;

  constructor(private otherService: OtherService) { }

  ngOnInit(): void {
    this.name = this.otherService.exercice7Data[this.index];
  }

  onEnableEdit(): void {
    this.enableEdit = true;
  }

  onEndEdit(): void {
    this.enableEdit = false;
  }

}
