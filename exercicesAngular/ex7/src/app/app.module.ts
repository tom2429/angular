import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Ex7Component } from './ex7/ex7.component';
import { OtherService } from './services/other.service';
import { InputModifierComponent } from './ex7/input-modifier/input-modifier.component';


@NgModule({
  declarations: [
    AppComponent,
    Ex7Component,
    InputModifierComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [    
    OtherService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
