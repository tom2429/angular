import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {

  quantite: number;

  constructor() { 
    this.quantite = 0;
  }

  incrementer() {
    this.quantite++
  }

  decrementer() {
    this.quantite--
  }

  ngOnInit(): void {
  }

}
