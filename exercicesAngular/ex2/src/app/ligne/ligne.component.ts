import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: '[app-ligne]',
  templateUrl: './ligne.component.html',
  styleUrls: ['./ligne.component.scss']
})
export class LigneComponent implements OnInit {

  @Input() id = 0;
  @Input() nom = "";
  @Input() prenom = "";
  @Input() mail = "";

  constructor() { }

  ngOnInit(): void {
  }

}
