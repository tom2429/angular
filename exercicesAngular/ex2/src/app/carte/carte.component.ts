import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-carte',
  templateUrl: './carte.component.html',
  styleUrls: ['./carte.component.scss']
})
export class CarteComponent implements OnInit {

  @Input() titre:string;
  @Input() sousTitre:string;
  @Input() description:string;
  @Input() lien:string;

  constructor() {
    this.titre="Mon titre";
    this.sousTitre="Un sous-titre";
    this.description="Blabla Blabla Blabla Blabla Blabla Blabla"
    this.lien="#"
   }

  ngOnInit(): void {
  }

}
